core = 8.x
api = 2
defaults[projects][subdir] = contrib

projects[admin_toolbar][type] = module
projects[admin_toolbar][version] = 1.27
projects[adminimal_admin_toolbar][type] = module
projects[adminimal_admin_toolbar][version] = 1.11
projects[adminimal_theme][type] = theme
projects[adminimal_theme][version] = 1.5
projects[apigee_edge][type] = module
projects[apigee_edge][version] = 1.15
projects[bootstrap_barrio][type] = theme
projects[bootstrap_barrio][version] = 4.22
projects[captcha][type] = module
projects[captcha][version] = 1.1
projects[contact_block][type] = module
projects[contact_block][version] = 1.5
projects[context][type] = module
projects[context][version] = 4.0-beta2
projects[ctools][type] = module
projects[ctools][version] = 3.4
projects[default_content][type] = module
projects[default_content][version] = 1.0-alpha9
projects[entity][type] = module
projects[entity][version] = 1.1
projects[key][type] = module
projects[key][version] = 1.14
projects[menu_item_role_access][type] = module
projects[menu_item_role_access][version] = 1.0
projects[page_manager][type] = module
projects[page_manager][version] = 4.0-beta6
projects[persistent_login][type] = module
projects[persistent_login][version] = 1.3
projects[recaptcha][type] = module
projects[recaptcha][version] = 2.5
projects[simplenews][type] = module
projects[simplenews][version] = 1.0-beta2
projects[token][type] = module
projects[token][version] = 1.7
projects[views_accordion][type] = module
projects[views_accordion][version] = 1.3
projects[weight][type] = module
projects[weight][version] = 3.2


